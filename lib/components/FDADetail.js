"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
// Copyright (c) 2018 SerialLab Corp.
//
// GNU GENERAL PUBLIC LICENSE
//    Version 3, 29 June 2007
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

const React = qu4rtet.require("react");
const Component = React.Component;

var _qu4rtet$require = qu4rtet.require("react-intl");

const FormattedMessage = _qu4rtet$require.FormattedMessage;

var _qu4rtet$require2 = qu4rtet.require("@blueprintjs/core");

const Card = _qu4rtet$require2.Card,
      Button = _qu4rtet$require2.Button,
      ControlGroup = _qu4rtet$require2.ControlGroup,
      InputGroup = _qu4rtet$require2.InputGroup,
      Tag = _qu4rtet$require2.Tag;

var _qu4rtet$require3 = qu4rtet.require("./components/layouts/Panels");

const RightPanel = _qu4rtet$require3.RightPanel;

var _qu4rtet$require4 = qu4rtet.require("react-redux");

const connect = _qu4rtet$require4.connect;

var _qu4rtet$require5 = qu4rtet.require("./plugins/pluginRegistration");

const pluginRegistry = _qu4rtet$require5.pluginRegistry;


const uuidv1 = qu4rtet.require("uuid/v1");

const yieldDataPairRowIfSet = (key, value) => {
  let improvedKey = key.replace(/_/g, " ").replace(/\w\S*/g, function (txt) {
    return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
  });
  let val = value;
  if (Array.isArray(value)) {
    val = value.join(", ");
  }
  if (typeof val === "string" && val.startsWith("<table")) {
    // don't show tables.
    return null;
  }
  if (key && value) {
    return React.createElement(
      "tr",
      null,
      React.createElement(
        "td",
        null,
        improvedKey
      ),
      React.createElement(
        "td",
        null,
        val
      )
    );
  }
  return null;
};

class _FDADetail extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const openfda = this.props.fdaItem.openfda;

    return React.createElement(
      RightPanel,
      { title: React.createElement(FormattedMessage, { id: "plugins.fda.FDALookup" }) },
      React.createElement(
        "div",
        { className: "twin-cards-container", style: { position: "relative" } },
        React.createElement(
          Card,
          { className: "pt-elevation-4" },
          React.createElement(
            "h5",
            null,
            openfda.brand_name,
            React.createElement(
              "button",
              {
                onClick: e => {
                  this.props.history.push(`/fda/${this.props.server.serverID}/map-trade-items/${this.props.match.params.fdaItem}`);
                },
                className: "pt-button pt-intent-primary add-incard-button" },
              React.createElement(FormattedMessage, { id: "plugins.fda.generateTradeItem" })
            )
          ),
          React.createElement(
            "table",
            {
              style: { paddingTop: "60px" },
              className: "pt-table data-pair-table pt-bordered pt-striped" },
            React.createElement(
              "tbody",
              null,
              Object.keys(openfda).map(item => {
                return yieldDataPairRowIfSet(item, openfda[item]);
              }),
              yieldDataPairRowIfSet("description", this.props.fdaItem.description),
              Object.keys(this.props.fdaItem).map(item => {
                if (item === "openfda") {
                  return null;
                }
                return yieldDataPairRowIfSet(item, this.props.fdaItem[item]);
              })
            )
          )
        )
      )
    );
  }
}

const FDADetail = exports.FDADetail = connect((state, ownProps) => {
  const isServerSet = () => {
    return state.fda.servers && state.fda.servers[ownProps.match.params.serverID];
  };
  let fdaServer = isServerSet() ? state.fda.servers[ownProps.match.params.serverID] : null;
  return {
    server: state.serversettings.servers[ownProps.match.params.serverID],
    fdaItem: fdaServer ? fdaServer.fdaItems[ownProps.match.params.fdaItem] : {}
  };
}, {})(_FDADetail);