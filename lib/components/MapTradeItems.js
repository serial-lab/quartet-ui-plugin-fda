"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.MapTradeItems = undefined;

var _ndcToGtin = require("../lib/ndcToGtin");

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; } // Copyright (c) 2018 SerialLab Corp.
//
// GNU GENERAL PUBLIC LICENSE
//    Version 3, 29 June 2007
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


const React = qu4rtet.require("react");
const Component = React.Component;

var _qu4rtet$require = qu4rtet.require("react-intl");

const FormattedMessage = _qu4rtet$require.FormattedMessage;

var _qu4rtet$require2 = qu4rtet.require("@blueprintjs/core");

const Card = _qu4rtet$require2.Card,
      Button = _qu4rtet$require2.Button,
      ControlGroup = _qu4rtet$require2.ControlGroup,
      InputGroup = _qu4rtet$require2.InputGroup,
      Tag = _qu4rtet$require2.Tag,
      FormGroup = _qu4rtet$require2.FormGroup,
      Switch = _qu4rtet$require2.Switch;

var _qu4rtet$require3 = qu4rtet.require("./components/layouts/Panels");

const RightPanel = _qu4rtet$require3.RightPanel;

var _qu4rtet$require4 = qu4rtet.require("react-redux");

const connect = _qu4rtet$require4.connect;

var _qu4rtet$require5 = qu4rtet.require("./plugins/pluginRegistration");

const pluginRegistry = _qu4rtet$require5.pluginRegistry;

const uuidv1 = qu4rtet.require("uuid/v1");

var _qu4rtet$require6 = qu4rtet.require("./lib/message");

const showMessage = _qu4rtet$require6.showMessage;


const yieldDataPairRowIfSet = (key, value) => {
  let improvedKey = key.replace(/_/g, " ").replace(/\w\S*/g, function (txt) {
    return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
  });
  let val = value;
  if (Array.isArray(value)) {
    val = value.join(", ");
  }
  if (val.startsWith("<table")) {
    // don't show tables.
    return null;
  }
  if (key && value) {
    return React.createElement(
      "tr",
      null,
      React.createElement(
        "td",
        null,
        improvedKey
      ),
      React.createElement(
        "td",
        null,
        val
      )
    );
  }
  return null;
};

class _MapTradeItems extends Component {
  constructor(props) {
    var _this;

    _this = super(props);

    this.enableToggle = index => {
      let items = [...this.state.items];
      items[index].enabled = !items[index].enabled;
      this.setState({ items });
    };

    this.selectIndicator = (index, evt) => {
      let items = [...this.state.items];
      items[index].indicatorDigit = Number(evt.target.value);
      items[index].gtin14 = (0, _ndcToGtin.NDCtoGTIN14)(items[index].packageNDC, items[index].indicatorDigit);
      this.setState({ items });
    };

    this.checkValIsSet = val => {
      return Array.isArray(val) && val.length > 0;
    };

    this.existingCompanyMatch = (() => {
      var _ref = _asyncToGenerator(function* (manufacturerName, ndc) {
        let serverObj = yield pluginRegistry.getServer(_this.props.server);
        let companyPrefix = (0, _ndcToGtin.NDCToCompanyPrefix)(ndc);
        let companyPrefixSearch = null;
        try {
          companyPrefixSearch = yield serverObj.fetchPageList("masterdata_companies_list", { search: companyPrefix });
        } catch (error) {
          // server seems not to pull list correctly.
          showMessage({
            id: "app.common.mainError",
            values: { msg: error.message },
            type: "warning"
          });
          return null;
        }
        if (companyPrefixSearch && companyPrefixSearch.count > 0) {
          for (let company of companyPrefixSearch.results) {
            if (company.gs1_company_prefix === companyPrefix) {
              // we have a perfect match, return id.
              return company.id;
            }
          }
        }
        // No perfect match. Don't use any of the companies found.
        return null;
      });

      return function (_x, _x2) {
        return _ref.apply(this, arguments);
      };
    })();

    this.getSetCompany = (() => {
      var _ref2 = _asyncToGenerator(function* (manufacturerName, ndc) {
        let serverObj = yield pluginRegistry.getServer(_this.props.server);
        let existingCompanyId = yield _this.existingCompanyMatch(manufacturerName, ndc);
        if (existingCompanyId) {
          // should match the company prefix, the company name, to be existing.
          return existingCompanyId;
        }
        let client = yield serverObj.getClient();
        try {
          let result = yield client.execute({
            operationId: "masterdata_companies_create",
            parameters: {
              data: {
                name: manufacturerName,
                gs1_company_prefix: (0, _ndcToGtin.NDCToCompanyPrefix)(ndc) || "unknown"
              }
            }
          });
          if (result.ok) {
            // we got a match post create.
            return result.body.id;
          } else {
            // something went wrong.
            showMessage({
              id: "app.common.mainError",
              values: { msg: "An error occurred while creating company" },
              type: "warning"
            });
          }
        } catch (error) {
          // create request didn't work.
          showMessage({
            id: "app.common.mainError",
            values: { msg: error.message },
            type: "warning"
          });
        }
      });

      return function (_x3, _x4) {
        return _ref2.apply(this, arguments);
      };
    })();

    this.capitalize = string => {
      return string.charAt(0).toUpperCase() + string.slice(1);
    };

    this.formatError = error => {
      try {
        let formattedMessage = Object.keys(error.response.body).map(key => {
          return `${this.capitalize(key.replace("_", " "))}: ${error.response.body[key].map(innerMsg => {
            return ` ${innerMsg}\n`;
          })}`;
        }).join(" ");
        showMessage({
          msg: formattedMessage,
          type: "warning",
          expires_in: 1000
        });
      } catch (e) {
        // ignore an error formatting an error.
        console.log("Error occurred while formatting error msg", e);
      }
    };

    this.generateTradeItems = (() => {
      var _ref3 = _asyncToGenerator(function* (evt) {
        evt.preventDefault();
        if (!_this.props.fdaItem.openfda.manufacturer_name && !_this.state.items[0].packageNDC) {
          showMessage({
            id: "app.common.mainError",
            values: { msg: "Missing Manufacturer and NDC data" },
            type: "warning"
          });
          return;
        }
        let client = yield pluginRegistry.getServer(_this.props.server).getClient();
        let company = yield _this.getSetCompany(_this.props.fdaItem.openfda.manufacturer_name[0], _this.state.items[0].packageNDC);
        _this.state.items.forEach(function (item, index) {
          let submitValues = { company: company };
          if (item.enabled) {
            if (_this.checkValIsSet(_this.props.fdaItem.description)) {
              submitValues.trade_item_description = _this.props.fdaItem.description[0].replace("DESCRIPTION", "").substring(0, 199);
            }
            if (_this.checkValIsSet(_this.props.fdaItem.openfda.manufacturer_name)) {
              submitValues.manufacturer_name = _this.props.fdaItem.openfda.manufacturer_name[0];
            }
            submitValues.GTIN14 = item.gtin14;
            submitValues.NDC = item.packageNDC;
            submitValues.NDC_pattern = (0, _ndcToGtin.getNDCPattern)(item.packageNDC);
            if (_this.checkValIsSet(_this.props.fdaItem.openfda.generic_name)) {
              submitValues.functional_name = _this.props.fdaItem.openfda.generic_name[0];
            }
            if (_this.checkValIsSet(_this.props.fdaItem.openfda.brand_name)) {
              submitValues.regulated_product_name = _this.props.fdaItem.openfda.brand_name[0];
            }
            setTimeout(function () {
              client.execute({
                operationId: "masterdata_trade_items_create",
                parameters: { data: submitValues }
              }).then(function (result) {
                if (result.ok) {
                  showMessage({
                    id: "app.common.objectCreatedSuccessfully",
                    values: { objectName: `GTIN ${submitValues.GTIN14}` },
                    type: "success"
                  });
                } else {
                  showMessage({
                    id: "app.common.mainError",
                    values: { msg: "An error occurred while creating trade item" },
                    type: "warning"
                  });
                }
              }).catch(function (error) {
                if (error.status === 400 && error.response && error.response.body) {
                  if (typeof error.response.body === "object" && error.response.body !== null) {
                    _this.formatError(error);
                  }
                } else {
                  showMessage({
                    id: "app.common.mainError",
                    values: { msg: error.message },
                    type: "warning"
                  });
                }
              });
            }, 200 * index);
          }
        });
      });

      return function (_x5) {
        return _ref3.apply(this, arguments);
      };
    })();

    this.state = { items: [] };
  }

  componentDidMount() {
    this.setState({
      items: this.props.fdaItem.openfda.package_ndc.map(item => {
        return {
          packageNDC: item,
          enabled: true,
          gtin14: (0, _ndcToGtin.NDCtoGTIN14)(item, 0),
          indicatorDigit: 0
        };
      })
    });
  }


  render() {
    const openfda = this.props.fdaItem.openfda;

    let intl = pluginRegistry.getIntl();
    return React.createElement(
      RightPanel,
      { title: React.createElement(FormattedMessage, { id: "plugins.fda.FDALookup" }) },
      React.createElement(
        "div",
        { className: "twin-cards-container", style: { position: "relative" } },
        React.createElement(
          Card,
          { className: "pt-elevation-4" },
          React.createElement(
            "h5",
            null,
            openfda.brand_name
          ),
          React.createElement(
            "form",
            {
              onSubmit: this.generateTradeItems,
              style: { marginTop: "30px" },
              className: "pt-form" },
            this.state.items.map((item, index) => {
              return React.createElement(
                "div",
                {
                  style: {
                    display: "flex",
                    justifyContent: "space-around",
                    alignItems: "center",
                    borderBottom: "1px solid #111",
                    padding: "20px"
                  } },
                React.createElement(Switch, {
                  checked: item.enabled,
                  label: intl.formatMessage({
                    id: "plugins.fda.generateTradeItem"
                  }),
                  onChange: this.enableToggle.bind(this, index)
                }),
                React.createElement(
                  FormGroup,
                  {
                    helperText: intl.formatMessage({
                      id: "plugins.fda.fdaProvidedNDC"
                    }),
                    label: `NDC #${index + 1}`,
                    labelFor: "text-input",
                    required: true },
                  React.createElement("input", {
                    id: "text-input",
                    disabled: true,
                    defaultValue: item.packageNDC,
                    className: "pt-input"
                  })
                ),
                React.createElement(
                  FormGroup,
                  {
                    helperText: intl.formatMessage({
                      id: "plugins.fda.indicatorDigit"
                    }),
                    label: intl.formatMessage({
                      id: "plugins.fda.indicatorDigit"
                    }),
                    labelFor: "text-input" },
                  React.createElement(
                    "div",
                    { "class": "pt-select" },
                    React.createElement(
                      "select",
                      {
                        value: item.indicatorDigit,
                        onChange: this.selectIndicator.bind(this, index) },
                      React.createElement(
                        "option",
                        { value: "0" },
                        "0"
                      ),
                      React.createElement(
                        "option",
                        { value: "1" },
                        "1"
                      ),
                      React.createElement(
                        "option",
                        { value: "2" },
                        "2"
                      ),
                      React.createElement(
                        "option",
                        { value: "3" },
                        "3"
                      ),
                      React.createElement(
                        "option",
                        { value: "4" },
                        "4"
                      ),
                      React.createElement(
                        "option",
                        { value: "5" },
                        "5"
                      ),
                      React.createElement(
                        "option",
                        { value: "6" },
                        "6"
                      ),
                      React.createElement(
                        "option",
                        { value: "7" },
                        "7"
                      ),
                      React.createElement(
                        "option",
                        { value: "8" },
                        "8"
                      ),
                      React.createElement(
                        "option",
                        { value: "9" },
                        "9"
                      )
                    )
                  )
                ),
                React.createElement(
                  FormGroup,
                  { helperText: "GTIN 14", label: "GTIN 14" },
                  React.createElement(
                    "span",
                    { style: { fontSize: "16px" } },
                    item.gtin14
                  )
                )
              );
            }),
            React.createElement(
              Button,
              {
                type: "submit",
                style: { marginTop: "30px" },
                className: "pt-intent-primary" },
              React.createElement(FormattedMessage, { id: "plugins.fda.generateTradeItems" })
            )
          )
        )
      )
    );
  }
}

const MapTradeItems = exports.MapTradeItems = connect((state, ownProps) => {
  const isServerSet = () => {
    return state.fda.servers && state.fda.servers[ownProps.match.params.serverID];
  };
  let fdaServer = isServerSet() ? state.fda.servers[ownProps.match.params.serverID] : null;
  return {
    server: state.serversettings.servers[ownProps.match.params.serverID],
    fdaItem: fdaServer ? fdaServer.fdaItems[ownProps.match.params.fdaItem] : {}
  };
}, {})(_MapTradeItems);