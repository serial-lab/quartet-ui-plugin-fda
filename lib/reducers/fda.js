"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.searchFDA = exports.initialData = undefined;

var _fda = require("../actions/fda");

var _fda2 = _interopRequireDefault(_fda);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _qu4rtet$require = qu4rtet.require("redux-actions"); // Copyright (c) 2018 SerialLab Corp.
//
// GNU GENERAL PUBLIC LICENSE
//    Version 3, 29 June 2007
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


const handleActions = _qu4rtet$require.handleActions;

var _qu4rtet$require2 = qu4rtet.require("./lib/reducer-helper");

const setServerState = _qu4rtet$require2.setServerState;

var _qu4rtet$require3 = qu4rtet.require("./lib/message");

const showMessage = _qu4rtet$require3.showMessage;

var _qu4rtet$require4 = qu4rtet.require("./plugins/pluginRegistration");

const pluginRegistry = _qu4rtet$require4.pluginRegistry;
const initialData = exports.initialData = () => {
  return {
    servers: {}
  };
};

const searchFDA = exports.searchFDA = (server, searchField, lookup, skip) => {
  return dispatch => {
    fetch(`https://api.fda.gov/drug/label.json?search=${searchField}:${lookup}&limit=20&skip=${skip}`).then(response => {
      if (!response.ok) {
        throw Error(response);
      }
      return response.json();
    }).then(data => {
      if (data.results && data.meta) {
        return dispatch({
          type: _fda2.default.loadFDAResults,
          payload: {
            serverID: server.serverID,
            searchField: searchField,
            lookup: lookup,
            items: data.results,
            skip: data.meta.results.skip,
            total: data.meta.results.total
          }
        });
      }
    }).catch(error => {
      return dispatch({
        type: _fda2.default.loadFDAResults,
        payload: {
          serverID: server.serverID,
          searchField: searchField,
          lookup: lookup,
          items: [],
          skip: 0,
          total: 0
        }
      });
      console.log(error);
    });
  };
};

exports.default = handleActions({
  [_fda2.default.loadFDAResults]: (state, action) => {
    return setServerState(state, action.payload.serverID, {
      fdaSearchField: action.payload.searchField,
      fdaLookup: action.payload.lookup,
      fdaItems: action.payload.items,
      fdaSkip: action.payload.skip,
      fdaTotal: action.payload.total
    });
  }
}, {});